# *Cornales* species complex data

In this directory you will find the following files:

1. `Cornales_R1.fastq.xz` - Compressed FASTQ data file for pair 1
2. `Cornales_R2.fastq.xz` - Compressed FASTQ data file for pair 2
2. `Cornales.barcodes` - Barcode file
3. `Cornales_info.md` - This file

---

The datafiles are compressed with XZ and have SHA256SUMs of `465c85382f295798ba8864c06d6d19579095d63bcacb65a302feee8db352d1e9` (R1) and `9e8dec499992ff388f7305eef19b89c7120ed99940065e3d24681d7f2aec932e` (R2). Use this hash to ensure your download was performed properly. The data is of the type "ddRAD", and is pair-ended. The restriction overhang for this specic library is `TGCAG`. Each file contains exactly 1 million reads which you should be able to assemble in your VM.

